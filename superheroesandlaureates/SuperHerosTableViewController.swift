//
//  SuperHerosTableViewController.swift
//  superheroesandlaureates
//
//  Created by Bheemireddy, Divyaharshini on 4/13/19.
//  Copyright © 2019 Bheemireddy, Divyaharshini. All rights reserved.
//

import UIKit

class SuperHerosTableViewController: UITableViewController {

    var superPowers : [String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        SuperHeroes.shared.fetchSuperHero()
        NotificationCenter.default.addObserver(self, selector: #selector(heroesRetrieved), name: Notification.Name("Heroes Retrieved"), object: nil)
    }
    
    @objc func heroesRetrieved(){
        DispatchQueue.main.sync {
            self.tableView.reloadData()
        }
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "SuperHeros"
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SuperHeroes.shared.members.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "superhero", for: indexPath)
        let heroes = SuperHeroes.shared[indexPath.row]
        superPowers = heroes.powers
        var Data:String = ""
        for i in 0..<superPowers.count{
            if i == superPowers.count-1{
                Data = Data + "\(superPowers[i])"
            }else{
                Data = Data + "\(superPowers[i]), "
            }
        }
        cell.textLabel?.text = "\(heroes.name) (AKA: \(heroes.secretIdentity))"
        cell.detailTextLabel?.text = Data
        return cell
    }

}

